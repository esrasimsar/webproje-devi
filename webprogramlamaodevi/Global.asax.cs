﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace webprogramlamaodevi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ControllerBuilder.Current.SetControllerFactory(new DefaultControllerFactory (new LocalizedControllerActivator()));
  }
        Public void Application_BeginRequest(Object sender, EventArgs e) 
     {     
 // Code that runs on application startup                                                            
         HttpCookie cookie = HttpContext.Current.Request.Cookies["CultureInfo"];
              if (cookie != null && cookie.Value != null) 
              {
                      System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cookie.Value);
                      System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cookie.Value);
               }
              else
              {
                      System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en");
                      System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en");

                  HttpCookie cookie_new = new HttpCookie("CurrentLanguage");
            cookie_new.Value = lang;
            Response.SetCookie(cookie_new);
        }

        base.InitializeCulture();
       }
}
    } 
      
    

