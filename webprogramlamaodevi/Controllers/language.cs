﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace webprogramlamaodevi.Controllers
{
    class languageAttribute : Attribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext )
        {
            string lang = filterContext.RouteData.Values["lang"] != null ? filterContext.RouteData.Values["lang"].ToString() : "tr";

            System.Threading.Thread.CurrentThread = new System.Globalization.CultureInfo(lang);
            System.Threading.Thread.CurrentThread = new System.Globalization.CultureInfo(lang);

            base.OnActionExecuting(filterContext);
        }
    }  
}
