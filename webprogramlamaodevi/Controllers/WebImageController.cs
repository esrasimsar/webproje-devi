﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace webprogramlamaodevi.Controllers
{
    public class WebImageController : Controller
    {
        // GET: WebImage
        public ActionResult Index()
        {
            return View();
        }
        public void resim()
        {
            WebImage resim1 = new WebImage("~/image/bebek3.jpg");
            resim1.Write();
        }
        public void resimgoruntule()
        {
            WebImage resim = new WebImage("~/image/bebek2.jpg");
            resim.Write();
        }
        public void resmiyenidenboyutlandır()
        {
            WebImage resim = new WebImage("~/image/bebek2.jpg");
            resim.Resize(10, 10, preserveAspectRatio: true);

            resim.Write();
        }
        public void annebeslenme()
        {
            WebImage beslenme = new WebImage("~/image/beslenme.jpg");
            beslenme.Write();
        }
        public void annespor()
        {
            WebImage spor = new WebImage("~/image/spor.jpg");
            spor.Write();
        }
        public void annepsıkolojı()
        {
            WebImage psikoloji = new WebImage("~/image/psikoloji.jpg");
            psikoloji.Write();
        }
        public void cocuk()
        {
            WebImage cocuk = new WebImage("~/image/cocuk.jpg");
            cocuk.Write();
        }
        public void cocuk1()
        {
            WebImage cocuk1 = new WebImage("~/image/cocuk1.jpg");
            cocuk1.Write();
        }
        public void cocukbeslenme()
        {
            WebImage cocukbeslenme = new WebImage("~/image/cocukbeslenme.jpg");
            cocukbeslenme.Write();
        }
        public void cocukspor()
        {
            WebImage cocukspor = new WebImage("~/image/cocukspor.jpg");
            cocukspor.Write();
        }
        public void psıkolog()
        {
            WebImage psıkolog = new WebImage("~/image/psıkolog1.jpg");
            psıkolog.Write();
        }
        public void psıkolog1()
        {
            WebImage psıkolog = new WebImage("~/image/psıkolog2.jpg");
            psıkolog.Write();
        }
        public void psıkolog2()
        {
            WebImage psıkolog = new WebImage("~/image/psıkolog3.jpg");
            psıkolog.Write();
        }
        public void bedensel()
        {
            WebImage bedensel = new WebImage("~/image/bedensel.jpg");
            bedensel.Write();
        }
        public void zıhınsel()
        {
            WebImage zıhınsel = new WebImage("~/image/zıhınsel.jpg");
            zıhınsel.Write();
        }


        public void elbecerı()
        {
            WebImage elbecerı = new WebImage("~/image/elbecerı.jpg");
            elbecerı.Write();
        }



      
    }
}