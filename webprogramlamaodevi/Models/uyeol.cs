﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webprogramlamaodevi.Models
{
    public class uyeol
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Cinsiyet { get; set; }
        public string Eposta { get; set; }
        public string Sifre { get; set; }

        public virtual ICollection<Kayıt> Uyeler { get; set;}


    }
}